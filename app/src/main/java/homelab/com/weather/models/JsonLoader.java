package homelab.com.weather.models;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;

public class JsonLoader implements Serializable {

    public JsonLoader() {
    }

    public String getJsonString(String url) throws IOException {
        String result = "";
        HttpURLConnection urlConnection;
        BufferedReader reader;
        try {
            URL urlka = new URL(url);
            urlConnection = (HttpURLConnection) urlka.openConnection();
            urlConnection.connect();

            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            reader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = reader.readLine()) != null) {
                buffer.append(line);
            }
            result = buffer.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }
}
