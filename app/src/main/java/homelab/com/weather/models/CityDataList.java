package homelab.com.weather.models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

public class CityDataList extends JsonLoader implements Serializable {

    public static final String JS_CITYDATA_TIME_DAY = "dt";
    public static final String JS_CITYDATA_TEMP_DAY = "day";
    public static final String JS_CITYDATA_TEMP_MIN = "min";
    public static final String JS_CITYDATA_TEMP_MAX = "max";
    public static final String JS_CITYDATA_TEMP_NIGHT = "night";
    public static final String JS_CITYDATA_TEMP_EVE = "eve";
    public static final String JS_CITYDATA_TEMP_MORN = "morn";
    public static final String JS_CITYDATA_PRESSURE = "pressure";
    public static final String JS_CITYDATA_HUMIDITY = "humidity";
    public static final String JS_CITYDATA_PRECIPITATION = "description";
    public static final String JS_CITYDATA_WIND_DIRECTION = "deg";
    public static final String JS_CITYDATA_WIND_SPEED = "speed";
    public static final String JS_CITYDATA_CLOUDS = "clouds";

    public CityDataList(String url, String nameCity) throws IOException, JSONException {
        parseJsonString(super.getJsonString(url), nameCity);
    }

    private ArrayList<CityItem> dataList = new ArrayList<>();

    public ArrayList<CityItem> getDataList() {
        return dataList;
    }

    private void parseJsonString(String jsonString, String nameCity) throws JSONException {
        JSONObject jsonObject = new JSONObject(jsonString);
        JSONArray cityDataArray = jsonObject.optJSONArray("list");
        for (int i = 0; i < cityDataArray.length(); i++) {
            JSONObject jsonDay = cityDataArray.optJSONObject(i);

            JSONObject cityDataObject2 = jsonDay.getJSONObject("temp");
            String strTempDay = cityDataObject2.optString(JS_CITYDATA_TEMP_DAY, "нет данных");
            String strTempMin  = cityDataObject2.optString(JS_CITYDATA_TEMP_MIN, "нет данных");
            String strTempMax = cityDataObject2.optString(JS_CITYDATA_TEMP_MAX, "нет данных");
            String strTempNight  = cityDataObject2.optString(JS_CITYDATA_TEMP_NIGHT, "нет данных");
            String strTempEve = cityDataObject2.optString(JS_CITYDATA_TEMP_EVE, "нет данных");
            String strTempMorn  = cityDataObject2.optString(JS_CITYDATA_TEMP_MORN, "нет данных");

            String strDesc = "";
            JSONArray cityPrecDataArray = jsonDay.optJSONArray("weather");
            for (int j = 0; j < cityPrecDataArray.length(); j++) {
                JSONObject jsonPrecDay = cityPrecDataArray.optJSONObject(j);
                strDesc = jsonPrecDay.optString(JS_CITYDATA_PRECIPITATION, "нет данных");
            }

            CityItem dataDay = new CityItem(
                    nameCity,
                    jsonDay.optString(JS_CITYDATA_TIME_DAY),
                    strTempDay,
                    strTempMin,
                    strTempMax,
                    strTempNight,
                    strTempEve,
                    strTempMorn,
                    jsonDay.optString(JS_CITYDATA_PRESSURE),
                    jsonDay.optString(JS_CITYDATA_HUMIDITY),
                    strDesc,
                    jsonDay.optString(JS_CITYDATA_WIND_DIRECTION),
                    jsonDay.optString(JS_CITYDATA_WIND_SPEED),
                    jsonDay.optString(JS_CITYDATA_CLOUDS)
            );
            dataList.add(dataDay);
        }
    }
}
