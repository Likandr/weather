package homelab.com.weather.models;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Serializable;

public class CityMoreList extends JsonLoader implements Serializable {
    public static final String JS_CITYDATA_TEMP = "temp";

    public static final String JS_CITYDATA_PRESSURE = "pressure";
    public static final String JS_CITYDATA_HUMIDITY = "humidity";
    public static final String JS_CITYDATA_TEMP_MAX = "temp_max";
    public static final String JS_CITYDATA_WIND_SPEED = "speed";
    public static final String JS_CITYDATA_DEG = "deg";


    public CityMoreList(String url, boolean onliTemp) throws IOException, JSONException {
        parseJsonString(super.getJsonString(url), onliTemp);
    }

    private String data = "";

    public String getDataDay() {
        return data;
    }

    private void parseJsonString(String jsonString, boolean onliTemp) throws JSONException {
        JSONObject jsonObject = new JSONObject(jsonString);
        if (jsonObject.optString("cod", "").equals("404")){
            data = " Нет такого города в базе";
        } else {
            JSONObject cityMain = jsonObject.getJSONObject("main");
            String strTemp = cityMain.optString(JS_CITYDATA_TEMP, "нет данных");
            String strPressure = cityMain.optString(JS_CITYDATA_PRESSURE, "нет данных");
            String strHumidity = cityMain.optString(JS_CITYDATA_HUMIDITY, "нет данных");
            String strTempMax = cityMain.optString(JS_CITYDATA_TEMP_MAX, "нет данных");
            JSONObject cityWind = jsonObject.getJSONObject("wind");
            String strWindSpeed = cityWind.optString(JS_CITYDATA_WIND_SPEED, "нет данных");
            String strDeg = cityWind.optString(JS_CITYDATA_DEG, "нет данных");

            if (onliTemp)
                data = " " + strTemp;
            else {
                data = "Температура: " + strTemp + "\n" +
                        "Давление: " + strPressure + "\n" +
                        "Влажность: " + strHumidity + "\n" +
                        "Макс. температура: " + strTempMax + "\n" +
                        "Скорость ветра: " + strWindSpeed + "\n" +
                        "Направление ветра: " + strDeg;
            }
        }

    }
}