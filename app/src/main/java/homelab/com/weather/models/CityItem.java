package homelab.com.weather.models;

import java.io.Serializable;

public class CityItem implements Serializable {

    private String mNameCity;
    private String mTimeDay;
    private String mTempDay;
    private String mTempMin;
    private String mTempMax;
    private String mTempNight;
    private String mTempEve;
    private String mTempMorn;
    private String mPressure;
    private String mHumidity;
    private String mDescription;
    private String mDeg;
    private String mSpeed;
    private String mLouds;

    public CityItem(String nameCity, String timeDay, String tempDay, String tempMin,
                    String tempMax, String tempNight, String tempEve, String tempMorn,
                    String pressure, String humidity, String desc, String deg, String speed,
                    String louds) {

        this.mNameCity = nameCity;
        this.mTimeDay = timeDay;
        this.mTempDay = tempDay;
        this.mTempMin = tempMin;
        this.mTempMax = tempMax;
        this.mTempNight = tempNight;
        this.mTempEve = tempEve;
        this.mTempMorn = tempMorn;
        this.mPressure = pressure;
        this.mHumidity = humidity;
        this.mDescription = desc;
        this.mDeg = deg;
        this.mSpeed = speed;
        this.mLouds = louds;
    }

    public String getNameCity() {
        return mNameCity;
    }

    public String getTimeDay() {
        return mTimeDay;
    }

    public String getTempDay() {
        return mTempDay;
    }

    public String getTempMin() {
        return mTempMin;
    }

    public String getTempMax() {
        return mTempMax;
    }

    public String getTempNight() {
        return mTempNight;
    }

    public String getTempEve() {
        return mTempEve;
    }

    public String getTempMorn() {
        return mTempMorn;
    }

    public String getPressure() {
        return mPressure;
    }

    public String getHumidity() {
        return mHumidity;
    }

    public String getDescription() {
        return mDescription;
    }

    public String getDeg() {
        return mDeg;
    }

    public String getSpeed() {
        return mSpeed;
    }

    public String getLouds() {
        return mLouds;
    }
}
