package homelab.com.weather;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;


public class FrEditCityList extends Fragment implements Const{
    SharedPreferences mSettings;
    private ListView lvCityData;
    private EditText etSearchNewCity;
    private Button btnSearch;
    private View v;

    private ArrayList<String> mListCityData = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fr_edit_city_list, container, false);
        mSettings = getActivity().getSharedPreferences(SHP, Context.MODE_PRIVATE);
        initViews();
        return v;
    }

    private void initViews() {
        mListCityData = new ArrayList<>();

        if (getArguments() != null) {
            for (int i = 0; i < getArguments().getInt("count"); i++)
                mListCityData.add(getArguments().getString("newcity" + i));
        } else {
            mListCityData.add("Moscow");
            mListCityData.add("Novaya%20Gollandiya");
        }

        etSearchNewCity = (EditText) v.findViewById(R.id.et_search_new_city);
        lvCityData = (ListView) v.findViewById(R.id.lv_city_list_edit);
        btnSearch = (Button) v.findViewById(R.id.btn_search);

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etSearchNewCity.getText().length() > 0) {
                    boolean inList = false;
                    for (int i = 0; i < lvCityData.getCount(); i++)
                        if (lvCityData.getItemAtPosition(i).equals(
                                etSearchNewCity.getText().toString().trim()))
                            inList = true;
                    if (!inList) {
                        mListCityData.add(etSearchNewCity.getText().toString().trim());
                        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(),
                                android.R.layout.simple_list_item_1, mListCityData);
                        lvCityData.setAdapter(adapter);
                    }
                }
            }
        });

        lvCityData.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                new AlertDialog.Builder(getActivity())
                        .setTitle("Удалить город из списка?")
                        .setMessage("Вы действительно хотите удалить из списка город '" +
                                lvCityData.getItemAtPosition(position).toString() + "'?")
                        .setNegativeButton(android.R.string.no, null)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface arg0, int arg1) {
                                mListCityData.remove(position);
                                ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(),
                                        android.R.layout.simple_list_item_1, mListCityData);
                                lvCityData.setAdapter(adapter);
                            }
                        }).create().show();
            }
        });

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_list_item_1, mListCityData);
        lvCityData.setAdapter(adapter);
    }

    @Override
    public void onDestroy() {
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putBoolean(SHP_CITYLIST_CHANGE, true);
        int count = 0;
        for (int i = 0; i < lvCityData.getCount(); i++) {
            editor.putString("newcity" + i, mListCityData.get(i).toString());
            count++;
        }
        editor.putInt("count", count);
        editor.apply();
        super.onDestroy();
    }
}
