package homelab.com.weather;

import android.app.ProgressDialog;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;

import homelab.com.weather.models.CityMoreList;

public class FrCity extends Fragment implements Const {
    View v;

    AsyncTask mLoaderDataCityTask;

    SharedPreferences mSettings;
    private ListView lvCity;
    private ArrayAdapter mAdapter;
    private ArrayList<String> mListCityData;
    boolean mFirstStart;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        mFirstStart = true;
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fr_city, container, false);

        lvCity = (ListView) v.findViewById(R.id.lv_city_list);
        mSettings = getActivity().getSharedPreferences(SHP, Context.MODE_PRIVATE);

        initCatalogData();

        lvCity.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> av, View view, int index,
                                    long arg3) {
                if (isConnected(getActivity())) {
                    String strOrig = lvCity.getItemAtPosition(index).toString();
                    String strRez = strOrig.substring(strOrig.indexOf(" "),
                            lvCity.getItemAtPosition(index).toString().length()).trim();
                    if (!strRez.equals(getResources().getString(R.string.str_city_not_found_in_db))) {
                        SharedPreferences.Editor editor = mSettings.edit();
                        editor.putString(SHP_CUR_CITY, lvCity.getItemAtPosition(index).toString().split(" ")[0]);
                        int count = 0;
                        for (int i = 0; i < lvCity.getCount(); i++) {
                            editor.putString("newcity" + i, mListCityData.get(i).toString().split(" ")[0]);
                            count++;
                        }
                        editor.putInt("count", count);
                        editor.apply();

                        Fragment fragment = new FrCityMore();
                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                        fragmentManager.beginTransaction()
                                .addToBackStack("frCityMore")
                                .replace(R.id.fl_contentFrame_activitymain, fragment)
                                .commit();
                    } else
                        Toast.makeText(getActivity(), "Города " +
                                lvCity.getItemAtPosition(index).toString().split(" ")[0] +
                                " НЕТ в базе", Toast.LENGTH_SHORT).show();
                } else
                    Toast.makeText(getActivity(), R.string.error_msg_network_lost, Toast.LENGTH_SHORT).show();
            }
        });
        return v;
    }

    private void initCatalogData() {
        getActivity().setTitle(R.string.app_name);

        if (mListCityData == null)
            mListCityData = new ArrayList<>();

        if (mFirstStart) {
            mListCityData.add("Moscow");
            mListCityData.add("Novaya%20Gollandiya");
            mFirstStart = false;
        }

        if (mSettings.contains("count")) {
            SharedPreferences.Editor editor = mSettings.edit();
            if (mSettings.getInt("count", 0) > 0) {
                mListCityData.clear();
                for (int i = 0; i < mSettings.getInt("count", 0); i++) {
                    mListCityData.add(mSettings.getString("newcity" + i, "Нет данных"));
                    editor.remove("newcity" + i);
                }
                mAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, mListCityData);
                lvCity.setAdapter(mAdapter);
            }
            editor.remove("count");
            editor.apply();
        }
        if (mAdapter == null) {
            mAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, mListCityData);
            lvCity.setAdapter(mAdapter);
        }
        mAdapter.notifyDataSetChanged();

        addTemperature();
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){
        inflater.inflate(R.menu.menu_editcity, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.btn_menu_editcity:
                editCity();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void editCity(){
        Fragment fragment = new FrEditCityList();
        Bundle bundle = new Bundle();
        for (int i = 0; i < lvCity.getCount(); i++) {
            bundle.putString("newcity" + i, mListCityData.get(i).toString().split(" ")[0]);
            bundle.putInt("count", i + 1);
        }
        fragment.setArguments(bundle);
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.fl_contentFrame_activitymain, fragment)
                .addToBackStack("frEditCityList")
                .commit();
    }

    public void addTemperature () {
        if (isConnected(getActivity()))
            mLoaderDataCityTask = new LoaderDataCity().execute();
        else
            Toast.makeText(getActivity(), getResources().getText(R.string.error_msg_network_lost), Toast.LENGTH_SHORT).show();
    }

    class LoaderDataCity extends AsyncTask<Void, Void, Void> {
        private ProgressDialog progressDialog;
        ArrayList<CityMoreList> arrayInfo = new ArrayList<>();
        int count = 0;

        @Override
        protected void onPostExecute(Void aVoid) {
            if (arrayInfo == null) {
                Toast.makeText(getActivity(), getResources().getText(R.string.error_msg_need_update_api_key), Toast.LENGTH_SHORT).show();
            } else {
                for (int i = 0; i < mListCityData.size(); i++) {
                    mListCityData.set(i, mListCityData.get(i) + "   " + arrayInfo.get(i).getDataDay());
                }

                if (mAdapter == null) {
                    mAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, mListCityData);
                    lvCity.setAdapter(mAdapter);
                }
                mAdapter.notifyDataSetChanged();
            }

            v.setVisibility(View.VISIBLE);
            progressDialog.dismiss();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            count++;
            v.setVisibility(View.INVISIBLE);
            progressDialog = new ProgressDialog(getActivity(), ProgressDialog.STYLE_SPINNER);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage(getResources().getString(R.string.msg_request_for_patience));
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
                try {
                    for (String city : mListCityData) {
                        arrayInfo.add(new CityMoreList(String.format(API_CURRENT, city, API_KEY), true));
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            return null;
        }
    }

    public static boolean isConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    @Override
    public void onDestroy() {
        mLoaderDataCityTask.cancel(true);
        super.onDestroy();
    }
}
