package homelab.com.weather;

public interface Const {

    String API_KEY = "cc3835bdac9edc08be06c2499f35026e";

    String API_CURRENT = "http://api.openweathermap.org/data/2.5/weather?" +
            "q=%1$s" +
            "&units=metric&appid=%2$s";

    String API_SEVERAL_DAY = "http://api.openweathermap.org/data/2.5/forecast/daily?" +
            "q=%1$s" +
            "&mode=json&units=metric&cnt=%2$s" +
            "&appid=%3$s";


    String SHP = "allSHPref";
    String SHP_CUR_CITY = "curCity";
    String SHP_CITYLIST_CHANGE = "changeCityList";
    String SHP_TREEDAYS = "days";
}
