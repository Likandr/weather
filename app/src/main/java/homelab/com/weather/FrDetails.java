package homelab.com.weather;

import android.support.v4.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;

import homelab.com.weather.database.DBEditor;
import homelab.com.weather.models.CityDataList;


public class FrDetails extends Fragment implements Const{

    private ListView lvCityData;
    private View v;

    AsyncTask mLoaderDataCityTask;

    SharedPreferences mSettings;
    private DBEditor dbEditor;
    private ArrayList<String> mListData = new ArrayList<>();
    private CityDataList cityDataList;
    boolean mFullWeek = false;

    String mCurrentCity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fr_details, container, false);
        lvCityData = (ListView) v.findViewById(R.id.lv_city_data);
        initViews();
        return v;
    }

    private void initViews() {
        mSettings = getActivity().getSharedPreferences(SHP, Context.MODE_PRIVATE);
        dbEditor = new DBEditor(getActivity());
        dbEditor.open();
        mCurrentCity = mSettings.getString(SHP_CUR_CITY, "");

        goLoadDataCity();
    }

    class LoaderDataCity extends AsyncTask<Void, Void, Void> {
        private ProgressDialog progressDialog;

        @Override
        protected void onPostExecute(Void aVoid) {
            if (dbEditor.searchCityDataInDB(mCurrentCity))
                dbEditor.deleteCityDataInDB(mCurrentCity);
            dbEditor.saveCityDataInDB(cityDataList);

            mListData.clear();
            mFullWeek = !mSettings.getBoolean(SHP_TREEDAYS, false);
            ArrayList<String> temp = dbEditor.getCityDataAdvertList(mCurrentCity, mFullWeek);
            if (mFullWeek) {
                mListData.addAll(temp);
            }else {
                for (int i = 0; i < 3; i++)
                    mListData.add(temp.get(i));
            }
            ArrayAdapter<String> adapter = new ArrayAdapter<>(
                    getActivity(), android.R.layout.simple_list_item_1,
                    mListData);
            lvCityData.setAdapter(adapter);

            v.setVisibility(View.VISIBLE);
            progressDialog.dismiss();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            v.setVisibility(View.INVISIBLE);
            progressDialog = new ProgressDialog(getActivity(), ProgressDialog.STYLE_SPINNER);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage(getResources().getString(R.string.msg_request_for_patience));
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                cityDataList = new CityDataList(
                        String.format(API_SEVERAL_DAY, mCurrentCity, 7, API_KEY), mCurrentCity);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){
        inflater.inflate(R.menu.menu_details, menu);
        super.onCreateOptionsMenu(menu, inflater);
        MenuItem item = menu.findItem(R.id.btn_menu_days);
        daysStateChange(item);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.btn_menu_days:
                if (mSettings.getBoolean(SHP_TREEDAYS, false)) {
                    SharedPreferences.Editor editor = mSettings.edit();
                    editor.putBoolean(SHP_TREEDAYS, false);
                    editor.apply();
                } else {
                    SharedPreferences.Editor editor = mSettings.edit();
                    editor.putBoolean(SHP_TREEDAYS, true);
                    editor.apply();
                }
                daysStateChange(item);
                goLoadDataCity();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void daysStateChange(MenuItem item){
        if (mSettings.getBoolean(SHP_TREEDAYS,false)) {
            item.setIcon(R.drawable.ic_ddd);
        } else {
            item.setIcon(R.drawable.ic_ddd_ddd);
        }
    }

    @Override
    public void onStop() {
        dbEditor.close();
        super.onStop();
    }

    @Override
    public void onDestroy() {
        mLoaderDataCityTask.cancel(true);
        super.onDestroy();
    }

    public void goLoadDataCity() {
        if (FrCity.isConnected(getActivity()))
            mLoaderDataCityTask = new LoaderDataCity().execute();
        else
            Toast.makeText(getActivity(), R.string.error_msg_network_lost, Toast.LENGTH_SHORT).show();
    }
}
