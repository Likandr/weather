package homelab.com.weather.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBHelper extends SQLiteOpenHelper {

    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "weather.db";

    public DBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createCityList(db);
        Log.d("DataBase", "База данных создана");
    }

    private void createCityList(SQLiteDatabase dataBase) {
        dataBase.execSQL("create table " + DBTableCityData.DB_TABLE_CITYDATA + "(" +
                DBTableCityData.DB_FIELD_ID           + " integer primary key autoincrement," +
                DBTableCityData.DB_FIELD_NAME_CITY    + " text, " +
                DBTableCityData.DB_FIELD_TIME_DAY   + " text, " +
                DBTableCityData.DB_FIELD_TEMP_DAY        + " text, " +
                DBTableCityData.DB_FIELD_TEMP_MIN         + " text, " +
                DBTableCityData.DB_FIELD_TEMP_MAX      + " text, " +
                DBTableCityData.DB_FIELD_TEMP_NIGHT  + " text, " +
                DBTableCityData.DB_FIELD_TEMP_EVE + " text, " +
                DBTableCityData.DB_FIELD_TEMP_MORN  + " text, " +
                DBTableCityData.DB_FIELD_PRESSURE  + " text, " +
                DBTableCityData.DB_FIELD_HUMIDITY  + " text, " +
                DBTableCityData.DB_FIELD_PRECIPITATION  + " text, " +
                DBTableCityData.DB_FIELD_WIND_DIRECTION  + " text, " +
                DBTableCityData.DB_FIELD_WIND_SPEED  + " text, " +
                DBTableCityData.DB_FIELD_CLOUDS          + " text);");

        Log.d("DataBase", "Таблица создана");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
