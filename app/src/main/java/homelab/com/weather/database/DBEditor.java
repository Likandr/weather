package homelab.com.weather.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import homelab.com.weather.models.CityItem;
import homelab.com.weather.models.CityDataList;

public class DBEditor {

    public static final String TAG = "DBEditor";

    private DBHelper mHelper;
    private SQLiteDatabase mDataBase;

    public DBEditor(Context context) {
        this.mHelper = new DBHelper(context);
    }

    public void open() {
        this.mDataBase = this.mHelper.getWritableDatabase();
        Log.d(TAG, "База данных открыта");
    }

    public void close() {
        if (this.mDataBase.isOpen()) {
            this.mDataBase.close();
            Log.d(TAG, "База данных закрыта");
        }
    }

    public boolean searchCityDataInDB(String cityName) {
        Cursor cursor = this.mDataBase.query(DBTableCityData.DB_TABLE_CITYDATA, null,
                "NAME = ?", new String[] {cityName}, null, null, null);
        int count = cursor.getCount();
        cursor.close();

        boolean tempState = false;
        if (count > 0)
            tempState = true;
        return tempState;
    }

    public Long saveCityDataInDB(CityDataList citydata) {
        Long id = new Long(0);
        if (citydata == null) {
            return -1l;
        }
        ArrayList<CityItem> cityDateArrList = citydata.getDataList();
        for (int i = 0; i < cityDateArrList.size(); i++) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(DBTableCityData.DB_FIELD_NAME_CITY, cityDateArrList.get(i).getNameCity());
            contentValues.put(DBTableCityData.DB_FIELD_TIME_DAY, cityDateArrList.get(i).getTimeDay());
            contentValues.put(DBTableCityData.DB_FIELD_TEMP_DAY, cityDateArrList.get(i).getTempDay());
            contentValues.put(DBTableCityData.DB_FIELD_TEMP_MIN, cityDateArrList.get(i).getTempMin());
            contentValues.put(DBTableCityData.DB_FIELD_TEMP_MAX, cityDateArrList.get(i).getTempMax());
            contentValues.put(DBTableCityData.DB_FIELD_TEMP_NIGHT, cityDateArrList.get(i).getTempNight());
            contentValues.put(DBTableCityData.DB_FIELD_TEMP_EVE, cityDateArrList.get(i).getTempEve());
            contentValues.put(DBTableCityData.DB_FIELD_TEMP_MORN, cityDateArrList.get(i).getTempMorn());
            contentValues.put(DBTableCityData.DB_FIELD_PRESSURE, cityDateArrList.get(i).getPressure());
            contentValues.put(DBTableCityData.DB_FIELD_HUMIDITY, cityDateArrList.get(i).getHumidity());
            contentValues.put(DBTableCityData.DB_FIELD_PRECIPITATION, cityDateArrList.get(i).getDescription());
            contentValues.put(DBTableCityData.DB_FIELD_WIND_DIRECTION, cityDateArrList.get(i).getDeg());
            contentValues.put(DBTableCityData.DB_FIELD_WIND_SPEED, cityDateArrList.get(i).getSpeed());
            contentValues.put(DBTableCityData.DB_FIELD_CLOUDS, cityDateArrList.get(i).getLouds());

            id = this.mDataBase.insert(DBTableCityData.DB_TABLE_CITYDATA, null, contentValues);
            Log.d(TAG, "ДАННЫЕ ПО ГОРОДУ " + cityDateArrList.get(i).getNameCity() + " ЗА ЧИСЛО: " +
                    cityDateArrList.get(i).getTimeDay() + " ДОБАВЛЕНЫ");
        }
        return id;
    }

    public int deleteCityDataInDB(String cityName) {
        int count = this.mDataBase.delete(DBTableCityData.DB_TABLE_CITYDATA,
                "NAME = ?", new String[] {cityName});
        Log.d(TAG, "Данные по городу " + cityName + " УДАЛЕНЫ");
        return count;
    }

    public ArrayList<String> getCityDataAdvertList(String cityName, boolean fullWeek) {
        ArrayList<String> advertList = new ArrayList<>();
        Cursor cursor = this.mDataBase.query(DBTableCityData.DB_TABLE_CITYDATA, null,
                "NAME = ?", new String[]{cityName}, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                String timeDay = cursor.getString(cursor.getColumnIndex(
                        DBTableCityData.DB_FIELD_TIME_DAY));
                String tempMin = cursor.getString(cursor.getColumnIndex(
                        DBTableCityData.DB_FIELD_TEMP_MIN));
                String tempMax = cursor.getString(cursor.getColumnIndex(
                        DBTableCityData.DB_FIELD_TEMP_MAX));
                String pressure = cursor.getString(cursor.getColumnIndex(
                        DBTableCityData.DB_FIELD_PRESSURE));
                String deg = cursor.getString(cursor.getColumnIndex(
                        DBTableCityData.DB_FIELD_WIND_DIRECTION));

                String strRes = "";
                try {
                    if (fullWeek) {
                        strRes = "Дата: " + modifyDateLayout(timeDay) + "\n" +
                                "Градусы(мин...макс)" + tempMin + " ... " + tempMax + "\n" +
                                "Направление ветра" + deg;
                    } else {
                        strRes = "Дата: " + modifyDateLayout(timeDay) + "\n" +
                                "Градусы(мин...макс): " + tempMin + " ... " + tempMax
                                + "\n" + "Направление ветра: " + deg + "\n" + " Давление: "
                                + pressure;
                    }
                }catch (Exception e){}
                advertList.add(strRes);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return advertList;
    }

    private String modifyDateLayout(String str) throws ParseException {
        long unixSeconds = Long.parseLong(str);
        Date date = new Date(unixSeconds * 1000L);
        SimpleDateFormat sdf = new SimpleDateFormat("dd - MM");
        String formattedDate = sdf.format(date);
        return formattedDate;
    }
}
