package homelab.com.weather.database;

public class DBTableCityData {

    public static final String DB_TABLE_CITYDATA = "city_data";

    public static final String DB_FIELD_ID = "_id";
    public static final String DB_FIELD_NAME_CITY = "name";
    public static final String DB_FIELD_TIME_DAY = "dt";
    public static final String DB_FIELD_TEMP_DAY = "day";
    public static final String DB_FIELD_TEMP_MIN = "min";
    public static final String DB_FIELD_TEMP_MAX = "max";
    public static final String DB_FIELD_TEMP_NIGHT = "night";
    public static final String DB_FIELD_TEMP_EVE = "eve";
    public static final String DB_FIELD_TEMP_MORN = "morn";
    public static final String DB_FIELD_PRESSURE = "pressure";
    public static final String DB_FIELD_HUMIDITY = "humidity";
    public static final String DB_FIELD_PRECIPITATION = "description";
    public static final String DB_FIELD_WIND_DIRECTION = "deg";
    public static final String DB_FIELD_WIND_SPEED = "speed";
    public static final String DB_FIELD_CLOUDS = "clouds";
}
