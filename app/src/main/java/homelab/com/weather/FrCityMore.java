package homelab.com.weather;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;

import java.io.IOException;

import homelab.com.weather.models.CityMoreList;

public class FrCityMore extends Fragment implements Const {
    private View v;
    private TextView tvShow;
    private Button btnShowMeMore;

    AsyncTask mLoaderDataCityTask;

    SharedPreferences mSettings;
    String mCurrentCity;
    CityMoreList mCityInfo;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fr_city_more, container, false);
        initCatalogData();
        return v;
    }

    private void initCatalogData() {
        tvShow = (TextView) v.findViewById(R.id.tv_citymore);
        btnShowMeMore = (Button) v.findViewById(R.id.btn_search);
        mSettings = getActivity().getSharedPreferences(SHP, Context.MODE_PRIVATE);

        mCurrentCity = mSettings.getString(SHP_CUR_CITY, "");
        getActivity().setTitle(mCurrentCity);

        btnShowMeMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new FrDetails();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .addToBackStack("frDetails")
                        .replace(R.id.fl_contentFrame_activitymain, fragment)
                        .commit();
            }
        });
        if (FrCity.isConnected(getActivity()))
            mLoaderDataCityTask = new LoaderDataCity().execute();
        else
            Toast.makeText(getActivity(), R.string.error_msg_network_lost, Toast.LENGTH_SHORT).show();
    }

    class LoaderDataCity extends AsyncTask<Void, Void, Void> {
        private ProgressDialog progressDialog;

        @Override
        protected void onPostExecute(Void aVoid) {
            tvShow.setText(mCityInfo.getDataDay());
            v.setVisibility(View.VISIBLE);
            progressDialog.dismiss();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            v.setVisibility(View.INVISIBLE);
            progressDialog = new ProgressDialog(getActivity(), ProgressDialog.STYLE_SPINNER);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage(getResources().getString(R.string.msg_request_for_patience));
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                mCityInfo = new CityMoreList(String.format(API_CURRENT, mCurrentCity, API_KEY), false);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    @Override
    public void onDestroy() {
        mLoaderDataCityTask.cancel(true);
        super.onDestroy();
    }
}
